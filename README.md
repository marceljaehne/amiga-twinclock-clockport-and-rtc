# Amiga Twinclock - Three Clockports and a RTC



## What is it?

This is an Expansion for all Amiga 500 Computers. It gives you three Clockport Ports and additionally a RTC.
The RTC can be seperatly activated/deactivated with SW1 Jumper.


![Alt-Text](https://gitlab.com/marceljaehne/amiga-twinclock-clockport-and-rtc/-/raw/main/Pictures/Twinclock.jpg)

Original Source: https://gitlab.com/marceljaehne/amiga-twinclock-clockport-and-rtc
